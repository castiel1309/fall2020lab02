//Castiel Le
//ID:1933080
package fall2020lab02;

public class BikeStore {
	private static Bicycle[] bikes = new Bicycle[4];
	public static void main(String[] args) {
		bikes[0] = new Bicycle("Raleigh", 1, 10);
		bikes[1] = new Bicycle("Giant", 3, 15);
		bikes[2] = new Bicycle("Cannondale", 6, 20);
		bikes[3] = new Bicycle("BMC", 7, 30);
		for(int i = 0; i < 4; i++) {
			System.out.println(bikes[i]);
		}
	}

}
