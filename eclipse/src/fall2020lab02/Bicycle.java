//Castiel Le
//ID: 1933080
package fall2020lab02;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	public String toString() {
		return ("Manufacturer: " + getManufacturer() + ", Number of Gears: " + getNumberGears() + ", MaxSpeed "
	+ getMaxSpeed());
	}
}
